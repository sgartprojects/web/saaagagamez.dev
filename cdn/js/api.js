update();

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function update(){
    /*// update Server - Debian
    var request = new XMLHttpRequest();
    request.open('GET', 'https://software.saaagagamez.dev/', true);
    request.onload = function () {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            document.getElementById("Debian").innerText = response.debian;
        } else {
            document.getElementById("Debian").innerText = "Offline";
        }
    }
    request.send();

    // update Server - Python
    var request = new XMLHttpRequest();
    request.open('GET', 'https://software.saaagagamez.dev/', true);
    request.onload = function () {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            document.getElementById("Python").innerText = response.python;
        } else {
            document.getElementById("Python").innerText = "Offline";
        }
    }
    request.send();

    // update Server - Pip
    var request = new XMLHttpRequest();
    request.open('GET', 'https://software.saaagagamez.dev/', true);
    request.onload = function () {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            document.getElementById("Pip").innerText = response.pip;
        } else {
            document.getElementById("Pip").innerText = "Offline";
        }
    }
    request.send();

    // update Server - Nginx
    var request = new XMLHttpRequest();
    request.open('GET', 'https://software.saaagagamez.dev/', true);
    request.onload = function () {
        var response = JSON.parse(this.responseText);
        if (this.readyState == 4 && this.status == 200 && response.nginx !== "") {
            document.getElementById("Nginx").innerText = response.nginx;
        } else if(this.readyState == 4 && this.status == 200 && response.nginx === ""){
            document.getElementById("Nginx").innerText = "1.14.2";
        } else {
            document.getElementById("Nginx").innerText = "Offline";
        }
    }
    request.send();*/

    // update Server - CPU
    var request = new XMLHttpRequest();
    request.open('GET', 'https://hardware.saaagagamez.dev/', true);
    request.onload = function () {
        var response = JSON.parse(this.responseText);
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("CPU").innerText = response.cpu + "%";
        } else {
            document.getElementById("CPU").innerText = "Offline";
        }
    }
    request.send();

    // update Server - RAM
    var request = new XMLHttpRequest();
    request.open('GET', 'https://hardware.saaagagamez.dev/', true);
    request.onload = function () {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            document.getElementById("Memory").innerText = formatBytes(response.ram.used, 1) + " / " + formatBytes(response.ram.total, 1);
        } else {
            document.getElementById("Memory").innerText = "Offline";
        }
    }
    request.send();
}

// update Server - Storage Root
/*var request = new XMLHttpRequest();
request.open('GET', 'https://stats.saaagagamez.dev', true);
request.onload = function() {
    if (this.readyState == 4 && this.status == 200) {
        var response = JSON.parse(this.responseText);
        document.getElementById("StorageRoot").innerText = formatBytes(response.hardware.storage.root.used, 1) + " / " + formatBytes(response.hardware.storage.root.total, 1);
    } else {
        document.getElementById("StorageRoot").innerText = "Offline";
    }
}
request.send();*/


// https://stackoverflow.com/questions/15900485/correct-way-to-convert-size-in-bytes-to-kb-mb-gb-in-javascript
function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}


